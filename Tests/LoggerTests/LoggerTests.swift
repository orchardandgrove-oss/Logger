import XCTest
@testable import Logger

final class LoggerTests: XCTestCase {
    func testSingleton() {
        XCTAssertNotNil(myLogger, "No myLogger singleton on system")
    }

    func testLogLevelEnum() {
        XCTAssertEqual(LogLevel.base.rawValue, 0, "base loglevel did not equal 0")
        XCTAssertEqual(LogLevel.info.rawValue, 1, "info loglevel did not equal 1")
        XCTAssertEqual(LogLevel.notice.rawValue, 2, "notice loglevel did not equal 2")
        XCTAssertEqual(LogLevel.debug.rawValue, 3, "debug loglevel did not equal 3")
    }

    func testDefaultLogLevel() {
        XCTAssertEqual(myLogger.loglevel, LogLevel.base, "Standard log level not set")
    }

    static var allTests = [
        ("testSingleton", testSingleton),
        ("testLogLevelEnum", testLogLevelEnum),
        ("testDefaultLogLevel", testDefaultLogLevel)
    ]
}
